import React, { Component } from "react";
import axios from "axios";
//import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";
import Loader from "./loader.gif";
//import NavBar from "./NavBar";
import PageNavigation from "./PageNavigation"
import "./Home.css";

class Search extends Component {
	constructor( props ) {
		super( props );
		
			this.state = {
			query: '',
			results: {},
			loading:  false,
			message:'',
			totalResults: 0,
			totalPages: 0,
			currentPageNo: 0,
	
			};
			this.cancel = '';
		}

		getPageCount = ( total, denominator ) => {
			const divisible	= 0 === total % denominator;
			const valueToBeAdded = divisible ? 0 : 1;
			return Math.floor( total/denominator ) + valueToBeAdded;
		};

		fetchSearchResult = ( updatedPageNo = '', query) => {
			const pageNumber = updatedPageNo ? `&page=${updatedPageNo}` : '';
			const searchUrl =`https://api.github.com/search/users?q=location:bangalore+${query}${pageNumber}`;

			if(this.cancel) {
				this.cancel.cancel();
			}

			this.cancel = axios.CancelToken.source();

			axios.get( searchUrl, {
				cancelToken:this.cancel.token
			} )
			.then( res => {
				const total = res.data.total;
				const totalPagesCount = this.getPageCount( total, 5 );
				const resultNotFoundMsg = ! res.data.items.length
										? 'there are no more search results. please try a new search'
										: '';
				this.setState({ 
					results: res.data.items,
					 message: resultNotFoundMsg, 
					 totalResults: total,
					totalPages: totalPagesCount,
					currentPageNo: updatedPageNo,
					loading:false })
			})

			.catch(error => {
				if ( axios.isCancel(error) || error ) {
					this.setState({ 
						loading: false,
						message: 'Failed to fetch data. Please check network'
					 })
				}
			})

		};

		handleonInputChange = ( event ) => {
			const query = event.target.value;
			if( !query ) {
				this.setState({ query:query, results: {}, message: '', totalPages: 0, totalResults: 0 });
			} else {
				this.setState( {query:query, loading: true, message: ''}, () =>{
					this.fetchSearchResult( 1,query );
			
			});
		}
		};
		handlePageClick = (event, type ) => {
			event.preventDefault();
			const updatePageNo = 'prev' === type
				? this.state.currentPageNo - 1
				: this.state.currentPageNo + 1;
	
			if( ! this.state.loading  ) {
				this.setState( { loading: true, message: '' }, () => {
					this.fetchSearchResults( updatePageNo, this.state.query );
				} );
			}
		};

		renderSearchResults = () => {
			const { results } = this.state;

			if ( Object.keys( results ).length && results.length ) {
				return(
					<div className="results-container">
					{ results.map( result => {
						return (
							<a key={result.id} href={ result.avatar_url } className="result-item">
							<h6 className="image-username">{result.login}</h6>
							<div className="image-wrapper">
								<img className="image" src={ result.avatar_url } alt={`${result.login} image`}/>	
							</div>
							</a>
						)
					})}
					</div>
				)
			}
		};

	render() {
		const {query,loading,message, currentPageNo, totalPages} =this.state;
		
		const showPrevLink = 1 < currentPageNo;
		const showNextLink = totalPages > currentPageNo;

		return (
			
			<div className="container">
			
			<h2 className="heading">Github Finder</h2>
			{/*Search component*/}
			<label className="search-label" htmlFor="search-input">
				<input
				type="text"
				name="query"
				placeholder="Search..."
				value={query}
				id="search-input"
				onChange= {this.handleonInputChange}
				/>
				<i className="fa fa-search search-icon" aria-hidden="true"/>
			</label>

			{/*	Error Message*/}
			{message && <p className="message">{ message }</p>}

			{/*	Loader*/}
			<img src={ Loader } className={`search-loading ${ loading ? 'show' : 'hide' }`} alt="loader"/>

			{/*Navigation*/}
			<PageNavigation
				loading={loading}
				showPrevLink={showPrevLink}
				showNextLink={showNextLink}
				handlePrevClick={ () => this.handlePageClick('prev' )}
				handleNextClick={ () => this.handlePageClick('next' )}
			/>

			{/* Results */}
				{ this.renderSearchResults() }
				
			{/*Navigation*/}
			<PageNavigation
			loading={loading}
			showPrevLink={showPrevLink}
			showNextLink={showNextLink}
			handlePrevClick={ () => this.handlePageClick('prev' )}
			handleNextClick={ () => this.handlePageClick('next')}
		/>
				
			</div>
		);
	}
}

export default Search;
