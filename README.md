# GithubFinder

A github finder with React.



## Features

-A MERN stack based web page which lists Github Users Based in 
 Bangalore.
 -The webpage should list users with picture,pagination and    search.
 -I can find users from this api:-
 https://api.github.com/search/users?q=location:bangalore



### Installing

Follow these steps to run this project in your local computer.

```
$ https://gitlab.com/gahlotdinesh/
$ cd Git Finder
$ npm i
```

Now, to run the project on port `3000`, run:

```
$ npm start
```

Go to `http://localhost:3000` to view the app.

## Built With

- [React.JS](https://reactjs.org/) -Frontend library used in the project.
- [Bootstrap](https://getbootstrap.com/) - Used for basic styling.


